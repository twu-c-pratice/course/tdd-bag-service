import java.util.HashMap;
import java.util.Map;

public class Cabinet {
    Map<Ticket, Bag> bigLockers = new HashMap<>();
    int bigLockerCapacity;

    public Cabinet(int bigLockerCapacity) {
        if (bigLockerCapacity < 1) {
            throw new IllegalArgumentException();
        }

        this.bigLockerCapacity = bigLockerCapacity;
    }

    public Ticket save(Bag bag, LockerSize lockerSize) {
        if (bag == null) {
            throw new IllegalArgumentException("Bag can not be null");
        }

        if (lockerSize == null) {
            throw new IllegalArgumentException();
        }

        if (bigLockerCapacity <= 0) {
            throw new InsufficientLockersException("Insufficient empty lockers");
        }

        Ticket ticket = new Ticket();

        switch (lockerSize) {
            case BIG:
                if (lockerSize.getSize() < bag.getBagSize().getSize()) {
                    throw new IllegalArgumentException();
                }
                bigLockers.put(ticket, bag);
                --bigLockerCapacity;
                break;
            default:
        }

        return ticket;
    }

    public Bag get(Ticket ticket) {
        if (ticket == null) {
            throw new IllegalArgumentException("Ticket can not be null");
        }
        if (!bigLockers.containsKey(ticket)) {
            throw new IllegalArgumentException("Invalid Ticket");
        }

        Bag bag = bigLockers.get(ticket);
        bigLockers.remove(ticket);
        ++bigLockerCapacity;
        return bag;
    }
}
