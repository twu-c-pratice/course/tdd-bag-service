public enum BagSize {
    HUGE(50),
    LARGE(40),
    MEDIUM(30),
    SMALL(20),
    MINI(10);

    private int size;

    BagSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
