public class Bag {
    private final BagSize bagSize;

    public Bag(BagSize bagSize) {
        if (bagSize == null) {
            throw new IllegalArgumentException();
        }
        this.bagSize = bagSize;
    }

    public BagSize getBagSize() {
        return bagSize;
    }
}
