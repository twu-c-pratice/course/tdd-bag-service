public enum LockerSize {
    BIG(40),
    MEDIUM(30);

    private int size;

    LockerSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
