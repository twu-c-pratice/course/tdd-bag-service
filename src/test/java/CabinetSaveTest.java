import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class CabinetSaveTest {

    Cabinet cabinet;
    Bag bag;
    Ticket ticket;

    @BeforeEach
    void setUp() {
        Integer capacity = Integer.MAX_VALUE;
        cabinet = new Cabinet(capacity);
        bag = new Bag(BagSize.LARGE);
        ticket = cabinet.save(bag, LockerSize.BIG);
    }

    @Test
    void should_get_a_ticket_when_save_the_bag() {
        assertNotNull(ticket);
        assertSame(ticket.getClass(), Ticket.class);
    }

    @Test
    void should_get_exception_when_save_null_to_the_cabinet() {
        assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.save(null, LockerSize.BIG),
                "Bag can not be null");
    }

    @Test
    void should_get_the_bag_using_the_ticket() {
        assertSame(bag, cabinet.get(ticket));
    }

    @Test
    void should_get_throw_when_using_ticket_generate_by_another_cabinet_to_get_bag() {
        Cabinet anotherCabinet = new Cabinet(Integer.MAX_VALUE);
        Ticket ticket = anotherCabinet.save(bag, LockerSize.BIG);

        assertThrows(IllegalArgumentException.class, () -> cabinet.get(ticket), "Invalid Ticket");
    }

    @Test
    void should_get_throw_when_using_null_to_get_bag() {
        assertThrows(
                IllegalArgumentException.class, () -> cabinet.get(null), "Ticket can not be null");
    }

    @Test
    void should_get_throw_if_the_ticket_is_used() {
        cabinet.get(ticket);
        assertThrows(IllegalArgumentException.class, () -> cabinet.get(ticket), "Invalid Ticket¬");
    }

    @Test
    void should_get_error_message_when_save_bag_with_null() {
        Integer capacity = Integer.MAX_VALUE;
        Cabinet cabinet = new Cabinet(capacity);

        assertThrows(
                IllegalArgumentException.class, () -> cabinet.save(new Bag(null), LockerSize.BIG));
    }

    @ParameterizedTest
    @EnumSource(
            value = BagSize.class,
            names = {"LARGE", "MEDIUM", "SMALL", "MINI"},
            mode = EnumSource.Mode.INCLUDE)
    void should_save_bags_to_big_locker_cabinet(BagSize bagSize) {
        bag = new Bag(bagSize);
        cabinet.save(bag, LockerSize.BIG);
    }

    @Test
    void should_throw_when_save_huge_bag_to_big_locker() {
        bag = new Bag(BagSize.HUGE);

        assertThrows(IllegalArgumentException.class, () -> cabinet.save(bag, LockerSize.BIG));
    }
}
