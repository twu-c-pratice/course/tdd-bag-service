import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CapacityTest {
    int capacity;
    Cabinet cabinet;

    @BeforeEach
    void setUp() {
        capacity = 3;
        cabinet = new Cabinet(capacity);
        cabinet.save(new Bag(BagSize.LARGE), LockerSize.BIG);
        cabinet.save(new Bag(BagSize.LARGE), LockerSize.BIG);
    }

    @Test
    void should_get_a_ticket_when_the_cabinet_is_not_full() {
        Ticket ticket = cabinet.save(new Bag(BagSize.LARGE), LockerSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_throw_error_when_save_bag_to_a_full_cabinet() {
        cabinet.save(new Bag(BagSize.LARGE), LockerSize.BIG);

        assertThrows(
                InsufficientLockersException.class,
                () -> cabinet.save(new Bag(BagSize.LARGE), LockerSize.BIG),
                "Insufficient empty lockers");
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0})
    void should_throw_error_when_the_init_capacity_is_less_then_one(int invalidCapacity) {
        assertThrows(IllegalArgumentException.class, () -> new Cabinet(invalidCapacity));
    }
}
